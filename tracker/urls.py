from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_to_list_projects(request):
    return redirect("list_projects")


urlpatterns = [
    path("tasks/", include("tasks.urls")),
    path("accounts/", include("accounts.urls")),
    path("", redirect_to_list_projects, name="home"),
    path("projects/", include("projects.urls")),
    path("admin/", admin.site.urls),
]
